<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">   
    <title>Arenera Playa Rica</title>
  </head>
  <style>
      input{
        box-shadow:none;
      }
      .card{
        width: 60%;
        margin-left: 20%;
        margin-right: 20%;
        margin-top: 10px;
      }
      #login-rest{
        width: 100%;  
        height:300px;
        overflow: scroll;         
        
        margin-top: 5px;
        

      }
      .btn-submit{
        background-color: #2E9AFE;
        color: #FFF;
        box-shadow: none;
      }
      #button{
        width:100%;
        border-style:solid;
        border-width:1px;
        border-radius:5px;
        border-color: #2E9AFE;        
        margin-top:10px;
        padding:10px;


                
      }
     
      #button:hover{
        background-color: #2E9AFE;
        border-color: #FFF;
        color: #FFF;
        cursor:pointer;
      }
      .alert{
        height:40px;
        margin-top:5px;
      }
      .nav-option{
        width: 60%;
        margin-left: 20%;
      }
  </style>
  <body>
  <div class="card">
    <h5 class="card-header">Consultar Certificado Generados</h5>
    <div class="card-body">
      <label for="formGroupExampleInput">Escribir un numero de certificado</label>   
      <input type="number" class="form-control" id="seearchc" name="searchc">          
      <div id="login-rest"></div>    
    </div>
  </div>
    <div class="nav-option">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="./index.php">Inicio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./addcliente.php">Agregar Cliente</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./greportetrimestral.php">Generar Reporte Trimestral</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./greporteclientes.php">Generar Reporte Cliente</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./actualizardatos.php">Actualizar Datos</a>
        </li>      
      </ul>
    </div>   
    
        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  
    <script>
      $(document).ready(function(){                               
              var url = "./controller/gcertificado/buscarcertificadog.php?numberc=0";
              loadDoc(url,"login-rest");
        
      });
      $('#seearchc').keyup(function(e){
            var numberc = $('#seearchc').val();
            if(numberc == ""){
              var numberc = "0";
            }
            var url = "./controller/gcertificado/buscarcertificadog.php?numberc="+numberc;
            loadDoc(url,"login-rest");
                        
        });

      function loadDoc(url,resp) {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
        document.getElementById(resp).innerHTML = this.responseText;
        }
      };
      xhttp.open("GET",url, true);
      xhttp.send();
    }

    </script>
  </body>
</html>