<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">   
    <title>Arenera Playa Rica</title>
  </head>
  <style>
      input{
        box-shadow:none;
      }
      .card{
        width: 60%;
        
        margin-left: 20%;
        margin-right: 20%;
        margin-top: 50px;
      }
      #login-rest{
        width: 60%;            
        margin-left: 20%;
        margin-right: 20%;
        margin-top: 5px;
        border-style:solid;
        border-width:1px;

      }
      .btn-submit{
        background-color: #2E9AFE;
        color: #FFF;
        box-shadow: none;
      }
      #button , #setbutton{
        width:100%;
        border-style:solid;
        border-width:1px;
        border-radius:5px;
        border-color: #2E9AFE;        
        margin-top:10px;
        padding:10px;


                
      }
      #button:hover , #setbutton:hover{
        background-color: #2E9AFE;
        border-color: #FFF;
        color: #FFF;
        cursor:pointer;

      }
      .nav-option{
        width: 60%;
        margin-left: 20%;
      }
  </style>
  <body>
    <div class="card">
    <h5 class="card-header">Generar Certificado de Origen</h5>
    <div class="card-body">             
        <form> 
            <label for="formGroupExampleInput">Seleccionar Cliente</label>
            <input type="text" class="form-control" id="ex1" name="setcliente" placeholder="!" requerid>
            <button id="setbutton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
              Buscar Cliente
            </button>
    
 
            <label for="formGroupExampleInput">Nombre del Conductor</label>
            <input type="text" class="form-control" id="ex2" name="namec" requerid>    

            <label for="formGroupExampleInput2">Placa del Vehiculo</label>
            <input type="text" class="form-control" id="ex3" name="placa" requerid>

            <label for="formGroupExampleInput3">Cantidad Mineral Comercializado</label>
            <input type="number" class="form-control" id="ex4" name="mineral" requerid>            
            <div id="button" class="text-center">Generar Certificado</div>        
        
    </div>
    </div>
    <div class="nav-option">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="./index.php">Inicio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./addcliente.php">Agregar Cliente</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./greportetrimestral.php">Generar Reporte Trimestral</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./greporteclientes.php">Generar Reporte Cliente</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./actualizardatos.php">Actualizar Datos</a>
        </li>      
      </ul>
    </div>
    <div id="login-rest"></div>
    <div  class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Busca Cliente</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <input type="text" class="form-control" id="seearchcliente" name="searchcliente" pattern="[A-Za-z]"> 
          <div id="datosrecibidos"></div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button id="scliente" type="button" class="btn btn-primary">Seleccionar Cliente</button>
          </div>
        </div>
      </div>
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>  
    <script>
      var idcliente

      $(document).ready(function(){
        var urls = "./controller/selectcliente.php?namecliente=' '";
        loadDoc(urls,"datosrecibidos");
        $('#button').click(function(){ 
              var idcliente = $("#ex1").val();
              var nameconductor = $("#ex2").val();
              var placa = $("#ex3").val();
              var mineral = $("#ex4").val();
              var url = "./controller/gcertificado/gcertificado.php?idcliente="+idcliente+"&nameconductor="+nameconductor+"&placa="+placa+"&mineral="+mineral;
              loadDoc(url,"login-rest");
              $('input[type="text"]').val('');
              $('input[type="number"]').val('');

        });
        $('#seearchcliente').keyup(function(e){
            var namecliente = $('#seearchcliente').val();
            var url = "./controller/selectcliente.php?namecliente="+namecliente;
            loadDoc(url,"datosrecibidos");
                        
        });
        $('#scliente').click(function(){
              var radiovalue = $("input[name='radiocliente']:checked").val();                          
              $('#ex1').val(radiovalue);      
              $('.fade').css("display","none");        
              
        });
      });




     function loadDoc(url,resp) {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
        document.getElementById(resp).innerHTML = this.responseText;
        }
      };
      xhttp.open("GET",url, true);
      xhttp.send();
    }

    </script>
  </body>
</html>