<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Arenera Playa Rica</title>
  </head>
  <style>
      .card{
        width: 40%;
        margin-left: 30%;
        margin-right: 30%;
        margin-top: 50px;
      }
      .card-body a{
        width: 100%;
        margin-bottom: 5px;
        background-color: #FFF;
        color: #000;

      }
  </style>
  <body>
    <div class="card">
    <h5 class="card-header text-center">Arenera Playa Rica</h5>
    <div class="card-body">         
        <a href="./gcertificado.php" class="btn btn-primary">Generar Certificado de Origen</a>
        <a href="./buscarcertificadosg.php" class="btn btn-primary">Ver Certificados de Origen</a>
        <a href="./greportetrimestral.php" class="btn btn-primary">Generar Reporte Trismestral</a>
        <a href="./greporteclientes.php" class="btn btn-primary">Generar Reporte  Cliente</a>
        <a href="./addcliente.php" class="btn btn-primary">Agregar Clientes</a>
        <a href="./actualizardatos.php" class="btn btn-primary">Actualizar Datos del Sistema</a>        
    </div>
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>