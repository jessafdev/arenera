<?php
    require '../libreria/fpdf.php';
    

    function gpdf($dia,$mes,$años,$placa,$rucom,$conductor,$consecutivo,$contidad){
    $pdf = new FPDF('L');
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',7);
    //Linea Vertical
    $pdf->SetLineWidth(0.5);
    $pdf->Line(150,10,150,189);
    $pdf->SetLineWidth(0.2);
    //TABLA IZQUIERDA----------------------------------------------------------
    $pdf->SetXY(10,10);
    $pdf->Cell(138,15,"",1);
    $pdf->Image('../../assets/logo-anm.jpg',60,11,40);
    $pdf->SetXY(10,25);
    $pdf->Cell(138,8,"CERTIFICADO DE ORIGEN EXPLOTADOR MINERO AUTORIZADO",1,"","C");    
    //->
    $pdf->SetXY(10,33);
    $pdf->Cell(22,10,"FECHA",1,"","C");
    $pdf->Cell(10,5,"DD",1,"","C");
    $pdf->Cell(10,5,"MM",1,"","C");
    $pdf->Cell(15,5,"AAAA",1,"","C");
    $pdf->SetXY(32,38);
    $pdf->Cell(10,5,$dia,1,"","C");
    $pdf->Cell(10,5,$mes,1,"","C");
    $pdf->Cell(15,5,$años,1,"","C");
    $pdf->SetXY(67,33);        
    $pdf->MultiCell(45,5,"No Consecutivo del Certificado de origen",1,"C");
    $pdf->SetXY(112,33);        
    $pdf->MultiCell(36,10,$consecutivo,1,"C");

    //<-

    $pdf->SetXY(10,43);
    $pdf->Cell(138,8,"INFORMACION DEL PRODUCTO DEL MINERAL",1,"","C");    
    $pdf->SetXY(10,51);
    $pdf->Cell(50,8,"EXPLOTADOR MINERO AUTORIZADO",1,"","C");        
    $pdf->Cell(88,8,"Titular Minero",1,"","C"); 
    $pdf->SetXY(10,59);   
    $pdf->Cell(50,8,"CODIGO EXPEDIENTE",1,"","C");        
    $pdf->Cell(88,8,"FLO-095",1,"","C");
    $pdf->SetXY(10,67);
    $pdf->MultiCell(50,4,"NOMBRES Y APELLIDOS O RAZON SOCIAL",1,"C"); 
    $pdf->SetXY(60,67);  
    $pdf->Cell(88,8,"JULIO RAFAEL ESTRADA SATIZABAL",1,"","C");   
    $pdf->SetXY(10,75);     
    $pdf->MultiCell(50,4,"TIPO DE IDENTIFICACION DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");    
    $pdf->SetXY(60,75);		       
    $pdf->MultiCell(88,8,"CEDULA",1,"C");  
    $pdf->SetXY(10,83);     
    $pdf->MultiCell(50,4,"No DOCUMENTO DE IDENTIDAD DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");    
    $pdf->SetXY(60,83);		       
    $pdf->MultiCell(88,8,"16,540,575",1,"C"); 
    $pdf->SetXY(10,91);     
    $pdf->MultiCell(50,4,"DEPARTAMENTO DONDE REALIZA LA EXPLOTACION",1,"C");    
    $pdf->SetXY(60,91);		       
    $pdf->MultiCell(88,8,"VALLE",1,"C"); 
    $pdf->SetXY(10,99);     
    $pdf->MultiCell(50,4,"MUNICIPIO(S) DONDE SE REALIZA LA EXPLOTACION",1,"C");    
    $pdf->SetXY(60,99);		       
    $pdf->MultiCell(88,8,"ROLDANILLO-ZARZAL",1,"C");  
    $pdf->SetXY(10,107);     
    $pdf->MultiCell(50,8,"MINERAL EXPLOTADO",1,"C");    
    $pdf->SetXY(60,107);		       
    $pdf->MultiCell(88,8,"MATERIALES DE CONSTRUCCION-ARENA DE RIO",1,"C");  
    $pdf->SetXY(10,115);     
    $pdf->MultiCell(50,4,"CANTIDAD MINERAL COMERCIALIZADO",1,"C");    
    $pdf->SetXY(60,115);		       
    $pdf->MultiCell(88,8,$contidad,1,"C");
    $pdf->SetXY(10,123);     
    $pdf->MultiCell(50,8,"UNIDAD DE MEDIDA",1,"C");    
    $pdf->SetXY(60,123);		       
    $pdf->MultiCell(88,8,"METROS CUBICO",1,"C");  
    $pdf->SetXY(10,131);
    $pdf->Cell(138,8,"INFORMACION DEL COMPRADOR DEL MINERAL",1,"","C");
    $pdf->SetXY(10,139);     
    $pdf->MultiCell(50,4,"NOMBRE Y APELLIDOS O RAZON SOCIAL",1,"C");    
    $pdf->SetXY(60,139);		       
    $pdf->MultiCell(88,8,"ARENERA PLAYA RICA S.A.S",1,"C");
    $pdf->SetXY(10,147);     
    $pdf->MultiCell(50,8,"TIPO DE INDENTIFICACION",1,"C");    
    $pdf->SetXY(60,147);		       
    $pdf->MultiCell(88,8,"NIT",1,"C"); 
    $pdf->SetXY(10,155);     
    $pdf->MultiCell(50,8,"COMPRADOR",1,"C");    
    $pdf->SetXY(60,155);		       
    $pdf->MultiCell(88,8,"COMERCILIZADOR",1,"C");    
    $pdf->SetXY(10,163);     
    $pdf->MultiCell(50,6,"No DOCUMENTO DE IDENTIDAD",1,"C");    
    $pdf->SetXY(60,163);		       
    $pdf->MultiCell(88,6,"Nit-900811452-0",1,"C"); 
    $pdf->SetXY(10,169);     
    $pdf->MultiCell(50,6,"No RUCOM",1,"C");    
    $pdf->SetXY(60,169);		       
    $pdf->MultiCell(88,6,$rucom,1,"C");  
    $pdf->SetXY(10,175);     
    $pdf->MultiCell(50,6,"CLIENTE Y PLACA",1,"C");    
    $pdf->SetXY(60,175);		       
    $pdf->MultiCell(40,6,$conductor,1,"C");
    $pdf->SetXY(100,175);               
    $pdf->MultiCell(48,6,$placa,1,"C"); 
    $pdf->SetXY(10,181);     
    $pdf->MultiCell(50,4,"FIRMA DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");    
    $pdf->SetXY(60,181);		       
    $pdf->MultiCell(88,8,"",1,"C");     
     
    
    
    

    //TABLA DERECHA
    $pdf->SetXY(152,10);
    $pdf->Cell(138,15,"",1);    
    $pdf->Image('../../assets/logo-anm.jpg',200,11,40);
    $pdf->SetXY(152,25);
    $pdf->Cell(138,8,"CERTIFICADO DE ORIGEN EXPLOTADOR MINERO AUTORIZADO",1,"","C");  

    //->
    $pdf->SetXY(152,33);
    $pdf->Cell(22,10,"FECHA",1,"","C"); 
    $pdf->Cell(10,5,"DD",1,"","C");
    $pdf->Cell(10,5,"MM",1,"","C");
    $pdf->Cell(15,5,"AAAA",1,"","C");
    $pdf->SetXY(174,38);
    $pdf->Cell(10,5,$dia,1,"","C");
    $pdf->Cell(10,5,$mes,1,"","C");
    $pdf->Cell(15,5,$años,1,"","C");
    $pdf->SetXY(209,33);        
    $pdf->MultiCell(45,5,"No Consecutivo del Certificado de origen",1,"C");
    $pdf->SetXY(254,33);        
    $pdf->MultiCell(36,10,$consecutivo,1,"C");
    
    //<-

    $pdf->SetXY(152,43);
    $pdf->Cell(138,8,"INFORMACION DEL PRODUCTOR DEL MINERAL",1,"","C");
    $pdf->SetXY(152,51);
    $pdf->Cell(50,8,"EXPLOTADOR MINERO AUTORIZADO",1,"","C");        
    $pdf->Cell(88,8,"Titular Minero",1,"","C");
    $pdf->SetXY(152,59);
    $pdf->Cell(50,8,"CODIGO EXPEDIENTE",1,"","C");        
    $pdf->Cell(88,8,"FLO-095",1,"","C");
    $pdf->SetXY(152,67);
    $pdf->MultiCell(50,4,"NOMBRES Y APELLIDOS O RAZON SOCIAL",1,"C"); 
    $pdf->SetXY(202,67);  
    $pdf->Cell(88,8,"JULIO RAFAEL ESTRADA SATIZABAL",1,"","C"); 
    $pdf->SetXY(152,75);  
    $pdf->MultiCell(50,4,"TIPO DE IDENTIFICACIÓN DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");
    $pdf->SetXY(202,75);  		       
    $pdf->MultiCell(88,8,"CEDULA",1,"C");
    $pdf->SetXY(152,83);  
    $pdf->MultiCell(50,4,"No DOCUMENTO DE IDENTIDAD DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");
    $pdf->SetXY(202,83);  		       
    $pdf->MultiCell(88,8,"16,540,575",1,"C");     
    $pdf->SetXY(152,91);  
    $pdf->MultiCell(50,4,"DEPARTAMENTO DONDE REALIZA LA EXPLOTACION",1,"C");
    $pdf->SetXY(202,91);  		       
    $pdf->MultiCell(88,8,"VALLE",1,"C");  
    $pdf->SetXY(152,99);  
    $pdf->MultiCell(50,4,"MUNICIPIO(S) DONDE REALIZA LA EXPLOTACION",1,"C");
    $pdf->SetXY(202,99);  		       
    $pdf->MultiCell(88,8,"ROLDANILLO-ZARZAL",1,"C");
    $pdf->SetXY(152,107);  
    $pdf->MultiCell(50,8,"MINERAL EXPLOTADO",1,"C");
    $pdf->SetXY(202,107);  		       
    $pdf->MultiCell(88,8,"MATERIALES DE CONSTRUCCION-ARENA DE RIO",1,"C");
    $pdf->SetXY(152,115);  
    $pdf->MultiCell(50,4,"CANTIDAD MINERAL COMERCIALIZADO",1,"C");
    $pdf->SetXY(202,115);  		       
    $pdf->MultiCell(88,8,$contidad,1,"C");
    $pdf->SetXY(152,123);  
    $pdf->MultiCell(50,8,"UNIDAD DE MEDIDA",1,"C");
    $pdf->SetXY(202,123);  		       
    $pdf->MultiCell(88,8,"METROS CUBICO",1,"C");
    $pdf->SetXY(152,131);
    $pdf->Cell(138,8,"INFORMACION DEL COMPRADOR DEL MINERAL",1,"","C");
    $pdf->SetXY(152,139);  
    $pdf->MultiCell(50,4,"NOMBRE Y APELLIDOS O RAZON SOCIAL",1,"C");
    $pdf->SetXY(202,139);  		       
    $pdf->MultiCell(88,8,"ARENERA PLAYA RICA S.A.S",1,"C");
    $pdf->SetXY(152,147);  
    $pdf->MultiCell(50,8,"TIPO DE INDENTIFICION",1,"C");
    $pdf->SetXY(202,147);  		       
    $pdf->MultiCell(88,8,"NIT",1,"C");
    $pdf->SetXY(152,155);  
    $pdf->MultiCell(50,8,"COMPRADOR",1,"C");
    $pdf->SetXY(202,155);  		       
    $pdf->MultiCell(88,8,"COMERCIALIZADOR",1,"C");
    $pdf->SetXY(152,163);  
    $pdf->MultiCell(50,6,"No DOCUMENTO DE IDENTIDAD",1,"C");
    $pdf->SetXY(202,163);  		       
    $pdf->MultiCell(88,6,"Nit-900811452-0",1,"C");
    $pdf->SetXY(152,169);  
    $pdf->MultiCell(50,6,"No RUCOM",1,"C");
    $pdf->SetXY(202,169);  		       
    $pdf->MultiCell(88,6,$rucom,1,"C");
    $pdf->SetXY(152,175);  
    $pdf->MultiCell(50,6,"CLIENTE Y PLACA",1,"C");
    $pdf->SetXY(202,175);  		       
    $pdf->MultiCell(40,6,$conductor,1,"C");
    $pdf->SetXY(242,175);              
    $pdf->MultiCell(48,6,$placa,1,"C");
    
    $pdf->SetXY(152,181);  
    $pdf->MultiCell(50,4,"FIRMA DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");
    $pdf->SetXY(202,181);  		       
    $pdf->MultiCell(88,8,"",1,"C");

    


  
    $pdf->Output();   
    

    }

   



?>