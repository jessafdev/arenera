<?php

	require '../../core/database/select.php';
	require '../../models/clientes.php';
	require '../../models/ventas.php';
	require '../libreria/fpdf.php';

	class PDF extends FPDF{
		function diastring($numero){
			if($numero <= 9){
				$diastring = "0".strval($numero);
				
			}
			if($numero >= 10){
				$diastring = strval($numero);
	
			}	
			return $diastring;	
	
		}
		function getdata($diastring,$mes,$years){
			$datos = ReportesVentasTrimestral($years."-".$mes."-".$diastring);
			if(sizeof($datos) > 0){
				
				for($contador2 = 0; $contador2 <= sizeof($datos)-1; $contador2++){
					$this->Cell(90,10,$years."-".$mes."-".$diastring,1,"","C");
					$this->Cell(90,10,$datos[$contador2][0],1,"","C");
					$this->Cell(100,10,$datos[$contador2][1],1,"","C");
					$this->Ln();
					
				    
				}				

			}
		}
		function getdatames($diasmes,$mes,$years){
			for($contador2 = 1; $contador2 <= $diasmes; $contador2++){					
				 $diastring = $this->diastring($contador2);
				 $this->getdata($diastring,$mes,$years);
			}
		}
		function getnumber($diastring,$mes,$years){
			$sumadia = 0;
			$datos = ReportesVentasTrimestral($years."-".$mes."-".$diastring);
			if(sizeof($datos) > 0){			
				for($contador2 = 0; $contador2 <= sizeof($datos)-1; $contador2++){			
					$sumadia = $sumadia + intval($datos[$contador2][1]);				    
				}	
			}else{
				$sumadia = $sumadia + 0;
			}
			return $sumadia;
		}
		function getsumames($diasmes,$mes,$years){
			$sumames = 0;
			for($contador2 = 1; $contador2 <= $diasmes; $contador2++){				
				$diastring = $this->diastring($contador2);
				$datosumar = $this->getnumber($diastring,$mes,$years);
				$sumames = $sumames  + $datosumar;
		   }
		 	return $sumames;
		}

	}

	$trimestre = $_GET["strimestre"];
	$years = $_GET["syears"];
	$contador=0;
	$fechainicialdia=0;
	$fechainicialmes=0;
	$fechafinaldia=0;
	$fechafinalmes=0;
	$datsumar= 0;


	$pdf = new PDF('L');
  $pdf->AddPage();
	$pdf->SetFont('Arial','B',11);
	$pdf->Cell(280,20,"REPORTE TRIMESTRAL",1,"","C");
	$pdf->ln();
	$pdf->Cell(90,15,"FECHA",1,"","C");
	$pdf->Cell(90,15,"CONSECUTIVO",1,"","C");
	$pdf->Cell(100,15,"CANTIDAD",1,"","C");
	
	
  if($trimestre == '1'){
		$suma1 = $pdf->getsumames(31,"01",$years);
		$suma2 = $pdf->getsumames(28,"02",$years);
		$suma3 = $pdf->getsumames(31,"03",$years);
		$sumat = $suma1 + $suma2 + $suma3;
		$pdf->ln();
		$pdf->Cell(280,10,"ENERO",1,"","C");
		$pdf->ln();
		$pdf->getdatames(31,"01",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma1),1,"","C");
		$pdf->ln();	
		$pdf->Cell(280,10,"FEBRERO",1,"","C");
		$pdf->ln();
		$pdf->getdatames(28,"02",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma2),1,"","C");	
		$pdf->ln();
		$pdf->Cell(280,10,"MARZO",1,"","C");
		$pdf->ln();
		$pdf->getdatames(31,"03",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma3),1,"","C");	
		$pdf->ln();
		$pdf->Cell(180,10,"SUMA TOTAL TRIMESTRAL",1,"","C");
		$pdf->Cell(100,10,strval($sumat),1,"","C");	

	}
	if($trimestre == '2'){
		$suma1 = $pdf->getsumames(30,"04",$years);
		$suma2 = $pdf->getsumames(31,"05",$years);
		$suma3 = $pdf->getsumames(30,"06",$years);
		$sumat = $suma1 + $suma2 + $suma3;
		$pdf->ln();
		$pdf->Cell(280,10,"ABRIL",1,"","C");
		$pdf->ln();
		$pdf->getdatames(30,"04",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma1),1,"","C");
		$pdf->ln();	
		$pdf->Cell(280,10,"MAYO",1,"","C");
		$pdf->ln();
		$pdf->getdatames(31,"05",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma2),1,"","C");	
		$pdf->ln();
		$pdf->Cell(280,10,"JUNIO",1,"","C");
		$pdf->ln();
		$pdf->getdatames(30,"06",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma3),1,"","C");	
		$pdf->ln();
		$pdf->Cell(180,10,"SUMA TOTAL TRIMESTRAL",1,"","C");
		$pdf->Cell(100,10,strval($sumat),1,"","C");	

	}
	if($trimestre == '3'){
		$suma1 = $pdf->getsumames(31,"07",$years);
		$suma2 = $pdf->getsumames(31,"08",$years);
		$suma3 = $pdf->getsumames(30,"09",$years);
		$sumat = $suma1 + $suma2 + $suma3;
		$pdf->ln();
		$pdf->Cell(280,10,"JULIO",1,"","C");
		$pdf->ln();
		$pdf->getdatames(31,"07",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma1),1,"","C");
		$pdf->ln();	
		$pdf->Cell(280,10,"AGOSTO",1,"","C");
		$pdf->ln();
		$pdf->getdatames(31,"08",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma2),1,"","C");	
		$pdf->ln();
		$pdf->Cell(280,10,"SEPTIEMBRE",1,"","C");
		$pdf->ln();
		$pdf->getdatames(30,"09",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma3),1,"","C");	
		$pdf->ln();
		$pdf->Cell(180,10,"SUMA TOTAL TRIMESTRAL",1,"","C");
		$pdf->Cell(100,10,strval($sumat),1,"","C");	

	}
	if($trimestre == '4'){
		$suma1 = $pdf->getsumames(31,"10",$years);
		$suma2 = $pdf->getsumames(30,"11",$years);
		$suma3 = $pdf->getsumames(31,"12",$years);
		$sumat = $suma1 + $suma2 + $suma3;
		$pdf->ln();
		$pdf->Cell(280,10,"OCTUBRE",1,"","C");
		$pdf->ln();
		$pdf->getdatames(31,"10",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma1),1,"","C");
		$pdf->ln();	
		$pdf->Cell(280,10,"NOVIEMBRE",1,"","C");
		$pdf->ln();
		$pdf->getdatames(30,"11",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma2),1,"","C");	
		$pdf->ln();
		$pdf->Cell(280,10,"DICIEMBRE",1,"","C");
		$pdf->ln();
		$pdf->getdatames(31,"12",$years);
		$pdf->Cell(180,10,"SUMA TOTAL MES",1,"","C");
		$pdf->Cell(100,10,strval($suma3),1,"","C");	
		$pdf->ln();
		$pdf->Cell(180,10,"SUMA TOTAL TRIMESTRAL",1,"","C");
		$pdf->Cell(100,10,strval($sumat),1,"","C");	

	}
	$pdf->Output();
	?>
	
	


	
	

 