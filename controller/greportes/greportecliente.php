<?php 
    require '../../core/database/select.php';
	require '../../models/clientes.php';
	require '../../models/ventas.php';

	$idcliente = $_GET["idcliente"];
	$fechainicial = $_GET["fechainicial"];
    $fechafinal = $_GET["fechafinal"];
    $contadorverificar = 0;


    



    if(empty($idcliente)){
    	echo datosvacios("Indentificacion o nit de cliente es requerido");
    }else{
    	$contadorverificar++;
    }
    if(empty($fechainicial)){
    	echo datosvacios("Fecha inicial es requerido");
    }else{
    	$contadorverificar++;
    }
    if(empty($fechafinal)){
    	echo datosvacios("Fecha final es requerido");
    }else{
    	$contadorverificar++;
    }

    if($contadorverificar == 3){
    	echo gtable(diasdatos(VentasCliente($idcliente,$fechainicial,$fechafinal)),SearchClienteNombre($idcliente));
    }else{
    	$contadorverificar = 0;
    }

    

    function VentasCliente($idcliente,$fechainicial,$fechafinal){
    	$table = "";
    	$datos = ReportesVentasClientes($idcliente,$fechainicial,$fechafinal);
    	if(sizeof($datos) > 0){
    		for($contador = 0; $contador <= sizeof($datos) - 1 ; $contador++){

    			$table = $table."<tr class='text-center'>										      
										      <td>".$datos[$contador][2]."</td>
										      <td>".$datos[$contador][0]."</td>
										      <td>".$datos[$contador][1]."</td>
										    </tr>";
    		}
    	}else{
    		echo datosvacios("NO HAY DATOS PARA ESTE CLIENTE");
    	}
    	return $table;
    	
    }
    function diasdatos($datosdias){
		$tabledatos = "<tr>						
						<tr class='text-center'>
					      <th scope='col'>FECHA</th>
					      <th scope='col'>CONSECUTIVO</th>
					      <th scope='col'>CANTIDAD</th>
					    </tr>
					     ".$datosdias."			
 						
					</tr>";
		return $tabledatos;
	}
	

    function gtable($datos,$cliente){
		$table = "<table class='table'>
		<thead>
			<tr>
				<th colspan='4' class='text-center table-report-title'>REPORTE POR CLIENTE</th>
			</tr>
			<tr>
				<th colspan='4' class='text-center'>NOMBRE DEL CLIENTE:".$cliente."</th>
			</tr>		    
		  </thead>
		  <tbody>
		    ".$datos."

		  </tbody>
		</table>";
		return $table;

	}

    function datosvacios($mensaje){
    	$men = "<div class='alert alert-warning text-center' role='alert'>".$mensaje."</div>";
    	return $men;
    }


  
?>