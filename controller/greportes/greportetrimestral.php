<?php

	require '../../core/database/select.php';
	require '../../models/clientes.php';
	require '../../models/ventas.php';

	$trimestre = $_GET["strimestre"];
	$years = $_GET["syears"];
	$contador=0;
	$fechainicialdia=0;
	$fechainicialmes=0;
	$fechafinaldia=0;
	$fechafinalmes=0;

	echo "<a class='gpdf' href='./controller/greportes/greportetrimestralpdf.php?strimestre=".$trimestre."&syears=".$years."' target='_black'>Exportar PDF</a>";
	
	
	
	


	if($trimestre == '1'){
		echo gtable(diasdatos(ventasclientes("01","31",$years),"ENERO"),$trimestre);
		echo gtable(diasdatos(ventasclientes("02","28",$years),"FEBRERO"),$trimestre);
		echo gtable(diasdatos(ventasclientes("03","31",$years),"MARZO"),$trimestre);

		$suma = ventassuma("01","31",$years) + ventassuma("02","28",$years) + ventassuma("03","31",$years);

		$table = "<table class='table'>		
		  <tbody>
		    <tr class='text-center table-report-title'>										      
										      <td colspan='2'>CANTIDAD TOTAL MENSUAL</td>
										      <td>".$suma."</td>
										      
										    </tr>

		  </tbody>
		</table>";
		echo $table;

		

		 



	}
	if($trimestre == '2'){
		echo gtable(diasdatos(ventasclientes("04","30",$years),"ABRIL"),$trimestre);
		echo gtable(diasdatos(ventasclientes("05","31",$years),"MAYO"),$trimestre);
		echo gtable(diasdatos(ventasclientes("06","30",$years),"JUNIO"),$trimestre);

		$suma = ventassuma("04","30",$years) + ventassuma("04","31",$years) + ventassuma("05","30",$years);

		$table = "<table class='table'>		
		  <tbody>
		    <tr class='text-center table-report-title'>										      
										      <td colspan='2'>CANTIDAD TOTAL SEMESTRAL</td>
										      <td>".$suma."</td>
										      
										    </tr>

		  </tbody>
		</table>";
		echo $table;



	}
	if($trimestre == '3'){
		echo gtable(diasdatos(ventasclientes("07","31",$years),"JULIO"),$trimestre);
		echo gtable(diasdatos(ventasclientes("08","31",$years),"AGOSTO"),$trimestre);
		echo gtable(diasdatos(ventasclientes("09","30",$years),"SEPTIEMBRE"),$trimestre);

		$suma = ventassuma("07","31",$years) + ventassuma("08","31",$years) + ventassuma("09","30",$years);

		$table = "<table class='table'>		
		  <tbody>
		    <tr class='text-center table-report-title'>										      
										      <td colspan='2'>CANTIDAD TOTAL SEMESTRAL</td>
										      <td>".$suma."</td>
										      
										    </tr>

		  </tbody>
		</table>";
		echo $table;



	}
	if($trimestre == '4'){
		echo gtable(diasdatos(ventasclientes("10","31",$years),"OCTUBRE"),$trimestre);
		echo gtable(diasdatos(ventasclientes("11","30",$years),"NOVIEMBRE"),$trimestre);
		echo gtable(diasdatos(ventasclientes("12","31",$years),"DICIEMBRE"),$trimestre);

		$suma = ventassuma("10","31",$years) + ventassuma("11","30",$years) + ventassuma("12","31",$years);

		$table = "<table class='table'>		
		  <tbody>
		    <tr class='text-center table-report-title'>										      
										      <td colspan='2'>CANTIDAD TOTAL SEMESTRAL</td>
										      <td>".$suma."</td>
										      
										    </tr>

		  </tbody>
		</table>";
		echo $table;



	}
	
	function diasdatos($datosdias,$mes){
		$tabledatos = "<tr> 
						<th colspan='4' class='text-center'>".$mes."</th>	
						<tr class='text-center'>
					      <th scope='col'>FECHA</th>
					      <th scope='col'>CONSECUTIVO</th>
					      <th scope='col'>CANTIDAD</th>
					    </tr>
					     ".$datosdias."					
 						
					</tr>";
		return $tabledatos;
	}
	

	function gtable($datos,$trimestre){
		$table = "<table class='table'>
		<thead>
			<tr>
				<th colspan='4' class='text-center table-report-title'>REPORTE TRIMESTRAL DE VENTAS</th>
			</tr>
			<tr>
				<th colspan='4' class='text-center'>TRIMESTRE #".$trimestre."</th>
			</tr>		    
		  </thead>
		  <tbody>
		    ".$datos."

		  </tbody>
		</table>";
		return $table;

	}
	
	function ventasclientes($mes,$diaf,$years){	
		$table = "";	
		$sumamensual=0;    	
		for($contador = 1 ; $contador <= intval($diaf) ; $contador++){
			if($contador <= 9){
				$diastring = "0".strval($contador);
				
			}
			if($contador >= 10){
				$diastring = strval($contador);

			}			
				
		
			
			$datos = ReportesVentasTrimestral($years."-".$mes."-".$diastring);
			if(sizeof($datos) > 0){
				for($contador2 = 0; $contador2 <= sizeof($datos)-1; $contador2++){
					$table = $table."<tr class='text-center'>										      
										      <td>".$years."-".$mes."-".$diastring."</td>
										      <td>".$datos[$contador2][0]."</td>
										      <td>".$datos[$contador2][1]."</td>
										    </tr>";
					$sumamensual = $sumamensual + intval($datos[$contador2][1]);
				    
				}				

			}
		}
		$table = $table."<tr class='text-center table-report-title'>										      
										      <td colspan='2'>CANTIDAD TOTAL MENSUAL</td>
										      <td>".strval($sumamensual)."</td>
										      
										    </tr>";

	    return $table;


	}

	function ventassuma($mes,$diaf,$years){	
		$sumamensual=0;    	
		for($contador = 1 ; $contador <= intval($diaf) ; $contador++){
			if($contador <= 9){
				$diastring = "0".strval($contador);
				
			}
			if($contador >= 10){
				$diastring = strval($contador);

			}			
				
		
			
			$datos = ReportesVentasTrimestral($years."-".$mes."-".$diastring);
			if(sizeof($datos) > 0){
				for($contador2 = 0; $contador2 <= sizeof($datos)-1; $contador2++){
					$sumamensual = $sumamensual + intval($datos[$contador2][1]);
				    
				}				

			}
		}
	

			return $sumamensual;
		
			
		


	}
	



	
	

	
  




 ?>