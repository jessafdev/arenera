<?php 

    
    require __DIR__.'/libreria/fpdf.php';
    require __DIR__.'/libreria/dash.php';
    function gpdf($dia,$mes,$años,$placa){
    $pdf = new PDF_Dash();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',7);
    $pdf->SetLineWidth(0.1);
/*$pdf->SetDash(5,5); //5mm on, 5mm off
$pdf->Line(20,20,190,20);
$pdf->SetLineWidth(0.5);
$pdf->Line(20,25,190,25);
$pdf->SetLineWidth(0.8);
$pdf->SetDash(4,2); //4mm on, 2mm off
$pdf->Rect(20,30,170,20);
$pdf->SetDash(); //restores no dash
$pdf->Line(20,55,190,55);

    $pdf->Cell(140,15,"",1);
    $pdf->Cell(140,15,"",1);
    $pdf->Image('img/logo-anm.jpg',60,11,40);
    $pdf->Image('img/logo-anm.jpg',200,11,40);
    $pdf->Ln();
    $pdf->Cell(140,8,"CERTIFICADO DE ORIGEN EXPLOTADOR MINERO AUTORIZADO",1,"","C");
    $pdf->Ln();
    $pdf->Cell(22,10,"FECHA",1,"","C"); //Parte Izquierda
    $pdf->Cell(10,5,"DD",1,"","C");
    $pdf->Cell(10,5,"MM",1,"","C");
    $pdf->Cell(15,5,"AAAA",1,"","C");
    $pdf->SetXY(32,38);
    $pdf->Cell(10,5,$dia,1,"","C");
    $pdf->Cell(10,5,$mes,1,"","C");
    $pdf->Cell(15,5,$años,1,"","C");
    $pdf->SetXY(67,33);        
    $pdf->MultiCell(45,5,"No Consecutivo del Certificado de origen",1,"C");
    $pdf->SetXY(112,33);        
    $pdf->MultiCell(38,10,"11",1,"C");

    $pdf->SetXY(150,25);
    $pdf->Cell(140,8,"CERTIFICADO DE ORIGEN EXPLOTADOR MINERO AUTORIZADO",1,"","C");        
    $pdf->SetXY(150,33);
    $pdf->Cell(22,10,"FECHA",1,"","C"); 
    $pdf->Cell(10,5,"DD",1,"","C");
    $pdf->Cell(10,5,"MM",1,"","C");
    $pdf->Cell(15,5,"AAAA",1,"","C");
    $pdf->SetXY(172,38);
    $pdf->Cell(10,5,$dia,1,"","C");
    $pdf->Cell(10,5,$mes,1,"","C");
    $pdf->Cell(15,5,$años,1,"","C");
    $pdf->SetXY(207,33);        
    $pdf->MultiCell(45,5,"No Consecutivo del Certificado de origen",1,"C");
    $pdf->SetXY(252,33);        
    $pdf->MultiCell(38,10,"11",1,"C");
    $pdf->SetXY(10,43);
    $pdf->Cell(140,8,"INFORMACION DEL PRODUCTO DEL MINERAL",1,"","C");
    $pdf->SetXY(150,43);
    $pdf->Cell(140,8,"INFORMACION DEL PRODUCTOR DEL MINERAL",1,"","C");
    $pdf->SetXY(10,51);
    $pdf->Cell(50,8,"EXPLOTADOR MINERO AUTORIZADO",1,"","C");        
    $pdf->Cell(90,8,"Titular Minero",1,"","C");       
    $pdf->Cell(50,8,"EXPLOTADOR MINERO AUTORIZADO",1,"","C");        
    $pdf->Cell(90,8,"Titular Minero",1,"","C");
    $pdf->Ln();
    $pdf->Cell(50,8,"CODIGO EXPEDIENTE",1,"","C");        
    $pdf->Cell(90,8,"FLO-095",1,"","C");    
    $pdf->Cell(50,8,"CODIGO EXPEDIENTE",1,"","C");        
    $pdf->Cell(90,8,"FLO-095",1,"","C");
    $pdf->SetXY(10,67);

    $pdf->MultiCell(50,4,"NOMBRES Y APELLIDOS O RAZON SOCIAL",1,"C");
    $pdf->SetXY(60,67);        
    $pdf->Cell(90,8,"JULIO RAFAEL ESTRADA SATIZABAL",1,"","C");  
    $pdf->SetXY(150,67);  
    $pdf->MultiCell(50,4,"NOMBRES Y APELLIDOS O RAZON SOCIAL",1,"C");   
    $pdf->SetXY(200,67);     
    $pdf->Cell(90,8,"JULIO RAFAEL ESTRADA SATIZABAL",1,"","C");  
    
    $pdf->SetXY(10,75);     
    $pdf->MultiCell(50,4,"TIPO DE IDENTIFICACION DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");    
    $pdf->SetXY(60,75);		       
    $pdf->MultiCell(90,8,"CEDULA",1,"C");  
    $pdf->SetXY(150,75);  
    $pdf->MultiCell(50,4,"TIPO DE IDENTIFICACIÓN DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");
    $pdf->SetXY(200,75);  		       
    $pdf->MultiCell(90,8,"CEDULA",1,"C");  

    $pdf->SetXY(10,83);     
    $pdf->MultiCell(50,4,"No DOCUMENTO DE IDENTIDAD DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");    
    $pdf->SetXY(60,83);		       
    $pdf->MultiCell(90,8,"16,540,575",1,"C");  
    $pdf->SetXY(150,83);  
    $pdf->MultiCell(50,4,"No DOCUMENTO DE IDENTIDAD DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");
    $pdf->SetXY(200,83);  		       
    $pdf->MultiCell(90,8,"16,540,575",1,"C");  

    $pdf->SetXY(10,91);     
    $pdf->MultiCell(50,4,"DEPARTAMENTO DONDE REALIZA LA EXPLOTACION",1,"C");    
    $pdf->SetXY(60,91);		       
    $pdf->MultiCell(90,8,"VALLE",1,"C");  
    $pdf->SetXY(150,91);  
    $pdf->MultiCell(50,4,"DEPARTAMENTO DONDE REALIZA LA EXPLOTACION",1,"C");
    $pdf->SetXY(200,91);  		       
    $pdf->MultiCell(90,8,"VALLE",1,"C");  

    $pdf->SetXY(10,99);     
    $pdf->MultiCell(50,4,"MUNICIPIO(S) DONDE SE REALIZA LA EXPLOTACION",1,"C");    
    $pdf->SetXY(60,99);		       
    $pdf->MultiCell(90,8,"ROLDANILLO-ZARZAL",1,"C");  
    $pdf->SetXY(150,99);  
    $pdf->MultiCell(50,4,"MUNICIPIO(S) DONDE REALIZA LA EXPLOTACION",1,"C");
    $pdf->SetXY(200,99);  		       
    $pdf->MultiCell(90,8,"ROLDANILLO-ZARZAL",1,"C");  

    $pdf->SetXY(10,107);     
    $pdf->MultiCell(50,8,"MINERAL EXPLOTADO",1,"C");    
    $pdf->SetXY(60,107);		       
    $pdf->MultiCell(90,8,"MATERIALES DE CONSTRUCCION-ARENA DE RIO",1,"C");  
    $pdf->SetXY(150,107);  
    $pdf->MultiCell(50,8,"MINERAL EXPLOTADO",1,"C");
    $pdf->SetXY(200,107);  		       
    $pdf->MultiCell(90,8,"MATERIALES DE CONSTRUCCION-ARENA DE RIO",1,"C");

    

    $pdf->SetXY(10,115);     
    $pdf->MultiCell(50,4,"CANTIDAD MINERAL COMERCIALIZADO",1,"C");    
    $pdf->SetXY(60,115);		       
    $pdf->MultiCell(90,8,"2335",1,"C");  
    $pdf->SetXY(150,115);  
    $pdf->MultiCell(50,4,"CANTIDAD MINERAL COMERCIALIZADO",1,"C");
    $pdf->SetXY(200,115);  		       
    $pdf->MultiCell(90,8,"2335",1,"C");

    $pdf->SetXY(10,123);     
    $pdf->MultiCell(50,8,"UNIDAD DE MEDIDA",1,"C");    
    $pdf->SetXY(60,123);		       
    $pdf->MultiCell(90,8,"METROS CUBICO",1,"C");  
    $pdf->SetXY(150,123);  
    $pdf->MultiCell(50,8,"UNIDAD DE MEDIDA",1,"C");
    $pdf->SetXY(200,123);  		       
    $pdf->MultiCell(90,8,"METROS CUBICO",1,"C");
    
    $pdf->SetXY(10,131);
    $pdf->Cell(140,8,"INFORMACION DEL COMPRADOR DEL MINERAL",1,"","C");
    $pdf->SetXY(150,131);
    $pdf->Cell(140,8,"INFORMACION DEL COMPRADOR DEL MINERAL",1,"","C");

    $pdf->SetXY(10,139);     
    $pdf->MultiCell(50,4,"NOMBRE Y APELLIDOS O RAZON SOCIAL",1,"C");    
    $pdf->SetXY(60,139);		       
    $pdf->MultiCell(90,8,"ARENERA PLAYA RICA S.A.S",1,"C");  
    $pdf->SetXY(150,139);  
    $pdf->MultiCell(50,4,"NOMBRE Y APELLIDOS O RAZON SOCIAL",1,"C");
    $pdf->SetXY(200,139);  		       
    $pdf->MultiCell(90,8,"ARENERA PLAYA RICA S.A.S",1,"C");

    $pdf->SetXY(10,147);     
    $pdf->MultiCell(50,8,"TIPO DE INDENTIFICACION",1,"C");    
    $pdf->SetXY(60,147);		       
    $pdf->MultiCell(90,8,"NIT",1,"C");  
    $pdf->SetXY(150,147);  
    $pdf->MultiCell(50,8,"TIPO DE INDENTIFICION",1,"C");
    $pdf->SetXY(200,147);  		       
    $pdf->MultiCell(90,8,"NIT",1,"C");

    $pdf->SetXY(10,155);     
    $pdf->MultiCell(50,8,"COMPRADOR",1,"C");    
    $pdf->SetXY(60,155);		       
    $pdf->MultiCell(90,8,"COMERCILIZADOR",1,"C");  
    $pdf->SetXY(150,155);  
    $pdf->MultiCell(50,8,"COMPRADOR",1,"C");
    $pdf->SetXY(200,155);  		       
    $pdf->MultiCell(90,8,"COMERCIALIZADOR",1,"C");

    $pdf->SetXY(10,163);     
    $pdf->MultiCell(50,6,"No DOCUMENTO DE IDENTIDAD",1,"C");    
    $pdf->SetXY(60,163);		       
    $pdf->MultiCell(90,6,"Nit-900811452-0",1,"C");  
    $pdf->SetXY(150,163);  
    $pdf->MultiCell(50,6,"No DOCUMENTO DE IDENTIDAD",1,"C");
    $pdf->SetXY(200,163);  		       
    $pdf->MultiCell(90,6,"Nit-900811452-0",1,"C");

    $pdf->SetXY(10,169);     
    $pdf->MultiCell(50,6,"No RUCOM",1,"C");    
    $pdf->SetXY(60,169);		       
    $pdf->MultiCell(90,6,"RUCOM-201708239898",1,"C");  
    $pdf->SetXY(150,169);  
    $pdf->MultiCell(50,6,"No RUCOM",1,"C");
    $pdf->SetXY(200,169);  		       
    $pdf->MultiCell(90,6,"RUCOM-201708239898",1,"C");

    $pdf->SetXY(10,175);     
    $pdf->MultiCell(50,6,"PLACA",1,"C");    
    $pdf->SetXY(60,175);		       
    $pdf->MultiCell(90,6,$placa,1,"C");  
    $pdf->SetXY(150,175);  
    $pdf->MultiCell(50,6,"PLACA",1,"C");
    $pdf->SetXY(200,175);  		       
    $pdf->MultiCell(90,6,$placa,1,"C");

    $pdf->SetXY(10,181);     
    $pdf->MultiCell(50,4,"FIRMA DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");    
    $pdf->SetXY(60,181);		       
    $pdf->MultiCell(90,8,"",1,"C");  
    $pdf->SetXY(150,181);  
    $pdf->MultiCell(50,4,"FIRMA DEL EXPLOTADOR MINERO AUTORIZADO",1,"C");
    $pdf->SetXY(200,181);  		       
    $pdf->MultiCell(90,8,"",1,"C");*/
    $pdf->Output();   
    

    }
    



?>
