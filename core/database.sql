create database areneraplayarica;
CREATE TABLE clientes(
     indentificacion VARCHAR(25) NOT NULL PRIMARY KEY,
     nombrecliente VARCHAR(50) NOT NULL

);
CREATE TABLE ventas(
     idventas INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
     idcliente VARCHAR(25) NOT NULL,
     nombreconductor VARCHAR(25) NOT NULL,
     consecutivo INT(11) NOT NULL,
     cantidad INT(11) NOT NULL,
     fecha DATETIME NOT NULL,
     fecha VARCHAR(20) NOT NULL,
     placa VARCHAR(10) NOT NULL,
     rucom VARCHAR(20) NOT NULL,
     dia VARCHAR(2),
     mes VARCHAR(2),
     ano VARCHAR(4),
     FOREIGN KEY(idcliente) REFERENCES clientes(indentificacion)
 
);
CREATE TABLE config(
     iddoc INT(11) NOT NULL PRIMARY KEY,
     numberc INT(11) NOT NULL,
     rucom varchar(20) NOT NULL,
     contrasena varchar(10) NOT NULL
);
